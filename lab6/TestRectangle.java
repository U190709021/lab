public class TestRectangle {
    public static void main(String[] args){
        Point topleft = new Point(10,10);

        Rectangle rect = new Rectangle(5,6,topleft);

        System.out.println("rectangle area:" + rect.area() + " rectangle perimeter:" + rect.perimeter());

        Point[] points = rect.corners();
        for(int i=0 ; i<points.length; i++){
            System.out.println("corner" +i+ " at x = "+points[i].xCoord+ " at y = "+points[i].yCoord);
        }
    }
}
