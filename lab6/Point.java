public class Point {
    int xCoord;
    int yCoord;

    public Point(int xCoord , int yCoord){
        this.xCoord =  xCoord;
        this.yCoord = yCoord;
    }
    public double distanceFromAPoint(Point point){
        int xdiff = xCoord-point.xCoord;
        int ydiff = yCoord-point.yCoord;
        return Math.sqrt(xdiff*xdiff + ydiff*ydiff);
    }
}
